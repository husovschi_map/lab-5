package MAP.Services;

import MAP.Models.CollegeYear;
import MAP.Models.Homework;
import MAP.Repositories.Repository;
import MAP.Validators.HomeworkValidator;
import MAP.Validators.ValidationException;

import java.time.LocalDateTime;

public class HomeworkService {
    private Repository<String, Homework> repository;
    private HomeworkValidator validator;

    public HomeworkService(Repository<String, Homework> repository, HomeworkValidator validator) {
        this.repository = repository;
        this.validator = validator;
    }

    public Homework delete(String id) {
        return this.repository.delete(id);
    }

    public Homework findOne(String id) {
        return this.repository.findOne(id);
    }

    public Iterable<Homework> findAll() {
        return this.repository.findAll();
    }

    public void save(String id, String deadLine, String description) {
        Homework homework = new Homework(id, CollegeYear.getInstance().getCurrentWeekOfSemester(LocalDateTime.now()),
                Integer.parseInt(deadLine), description);
        try {
            this.validator.validate(homework);
            this.repository.save(homework);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void update(String id, String deadLine, String description) {
        Homework homework = this.repository.findOne(id);
        Homework homework1 = homework; // How do java Object behave here?
        if (Integer.parseInt(deadLine) < CollegeYear.getInstance().getCurrentWeekOfSemester(LocalDateTime.now()))
            return;
        homework1.setDescription(description);
        homework1.setDeadLine(Integer.parseInt(deadLine));

        try {
            this.validator.validate(homework1);
            this.repository.update(homework, homework1);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
