package MAP.Models;

import java.time.LocalDate;

public class Semester extends Entity<String> {

    private LocalDate startSemester;
    private LocalDate startHoliday;
    private LocalDate endHoliday;
    private LocalDate endSemester;

    private static Semester instance = null;

    public Semester(String id) {
        super(id);
        switch (id) {
            case "1":
                this.startSemester = LocalDate.of(2019, 9, 30);
                this.startHoliday = LocalDate.of(2019, 12, 23);
                this.endHoliday = LocalDate.of(2020, 1, 5);
                this.endSemester = LocalDate.of(2020, 1, 17);
                break;
            case "2":
                this.startSemester = LocalDate.of(2020, 2, 24);
                this.startHoliday = LocalDate.of(2020, 4, 20);
                this.endHoliday = LocalDate.of(2020, 4, 26);
                this.endSemester = LocalDate.of(2020, 6, 5);
        }
    }

    public LocalDate getStartSemester() {
        return startSemester;
    }

    public void setStartSemester(LocalDate startSemester) {
        this.startSemester = startSemester;
    }

    public LocalDate getStartHoliday() {
        return startHoliday;
    }

    public void setStartHoliday(LocalDate startHoliday) {
        this.startHoliday = startHoliday;
    }

    public LocalDate getEndHoliday() {
        return endHoliday;
    }

    public void setEndHoliday(LocalDate endHoliday) {
        this.endHoliday = endHoliday;
    }

    public LocalDate getEndSemester() {
        return endSemester;
    }

    public void setEndSemester(LocalDate endSemester) {
        this.endSemester = endSemester;
    }
}

