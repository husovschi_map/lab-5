package MAP.Validators;

import MAP.Models.Grade;

public class GradeValidator implements Validator<Grade> {
    @Override
    public void validate(Grade grade) throws ValidationException {
        String string = "";
        if (grade.getId().equals(""))
            string += "Incorect data! ID is invalid!\n";

        if ((grade.getValue() < 1) || (grade.getValue() > 10))
            string += "Incorect data! Value is invalid!\n";

        if (grade.getTeacher().equals(""))
            string += "Incorect data! Teacher is invalid!\n";

        if (string.length() > 0)
            throw new ValidationException(string);
    }
}
