package MAP.Repositories.XML;

import MAP.Models.Entity;
import MAP.Models.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class StudentRepositoryXML extends AbstractXMLRepository {
    public StudentRepositoryXML(String fileName) {
        super(fileName);
    }

    @Override
    public Entity createEntityFromElement(Element element) {
        String id = element.getElementsByTagName("id")
                .item(0)
                .getTextContent();

        String firstName = element.getElementsByTagName("firstName")
                .item(0)
                .getTextContent();

        String lastName = element.getElementsByTagName("lastName")
                .item(0)
                .getTextContent();

        String group = element.getElementsByTagName("group")
                .item(0)
                .getTextContent();

        String email = element.getElementsByTagName("email")
                .item(0)
                .getTextContent();

        String labTeacher = element.getElementsByTagName("labTeacher")
                .item(0)
                .getTextContent();

        return new Student(id, firstName, lastName, group, email, labTeacher);
    }

    @Override
    public Element createElementFromEntity(Document document, Entity entity) {
        Student student = (Student) entity;

        Element element = document.createElement("id");
        element.setAttribute("id", student.getId());

        Element firstName = document.createElement("firstName");
        firstName.setAttribute("firstName", student.getFirstName());
        element.appendChild(firstName);

        Element lastName = document.createElement("lastName");
        lastName.setAttribute("lastName", student.getFirstName());
        element.appendChild(lastName);

        Element group = document.createElement("group");
        group.setAttribute("group", student.getFirstName());
        element.appendChild(group);

        Element email = document.createElement("email");
        email.setAttribute("email", student.getFirstName());
        element.appendChild(email);

        Element labTeacher = document.createElement("labTeacher");
        labTeacher.setAttribute("labTeacher", student.getFirstName());
        element.appendChild(labTeacher);

        return element;
    }
}
