package MAP.Models;

import java.time.LocalDateTime;

public class Grade extends Entity<IdGrade> {

    private LocalDateTime date;
    private String teacher;
    private int value;

    public Grade(IdGrade id, LocalDateTime date, String teacher, int value) {
        super(id);
        this.date = date;
        this.teacher = teacher;
        this.value = value;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "date=" + date +
                ", teacher='" + teacher + '\'' +
                ", value=" + value +
                '}';
    }
}
