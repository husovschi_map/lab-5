package MAP.Validators;

import MAP.Models.Homework;

public class HomeworkValidator implements Validator<Homework> {
    @Override
    public void validate(Homework homework) throws ValidationException {
        String string = "";

        if (homework.getId().equals(""))
            string += "Incorect data! ID is invalid!\n";

        if (homework.getStartWeek() < 1 || homework.getStartWeek() > 14)
            string += "Incorect data! StartWeek is invalid!\n";

        if (homework.getDeadLine() < 1 || homework.getDeadLine() > 14)
            string += "Incorect data! DeadLineWeek is invalid!\n";


        if (homework.getStartWeek() > homework.getDeadLine())
            string += "Incorect data! StartWeek is greater than DeadLineWeek!\n";

        if (string.length() > 0)
            throw new ValidationException(string);
    }
}
