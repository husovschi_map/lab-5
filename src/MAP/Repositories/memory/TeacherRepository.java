package MAP.Repositories.memory;

import MAP.Models.Teacher;

public class TeacherRepository extends AbstractRepository<String, Teacher> {

    public TeacherRepository() {
        super();
    }
}