package MAP.Services;

import MAP.Models.Teacher;
import MAP.Repositories.Repository;
import MAP.Validators.TeacherValidator;
import MAP.Validators.ValidationException;

public class TeacherService {
    private Repository<String, Teacher> repository;
    private TeacherValidator teacherValidator;

    public TeacherService(Repository<String, Teacher> repository, TeacherValidator teacherValidator) {
        this.repository = repository;
        this.teacherValidator = teacherValidator;
    }

    public void save(String id, String firstName, String lastName, String email) {
        Teacher teacher = new Teacher(id, firstName, lastName, email);
        try {
            this.teacherValidator.validate(teacher);
            this.repository.save(teacher);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    public void update(String id, String firstName, String lastName, String email) {
        Teacher teacher = this.repository.findOne(id);
        if (teacher != null) {
            Teacher teacher1 = teacher;
            teacher1.setFirstName(firstName);
            teacher1.setLastName(lastName);
            teacher1.setEmail(email);
            //TODO
        }
    }

    public Teacher findOne(String id) {
        return this.repository.findOne(id);
    }

    public Teacher delete(String id) {
        return this.repository.delete(id);
    }

    public Iterable<Teacher> findAll() {
        return this.repository.findAll();
    }
}
