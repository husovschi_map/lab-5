package MAP.Repositories.memory;

import MAP.Models.Grade;
import MAP.Models.IdGrade;

public class GradeRepository extends AbstractRepository<IdGrade, Grade> {

    public GradeRepository() {
        super();
    }
}
