package MAP.Repositories.memory;

import MAP.Models.Student;

public class StudentRepository extends AbstractRepository<String, Student> {

    public StudentRepository() {
        super();
    }
}