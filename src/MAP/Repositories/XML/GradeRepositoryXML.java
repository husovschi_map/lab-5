package MAP.Repositories.XML;

import MAP.Models.Entity;
import MAP.Models.Grade;
import MAP.Models.IdGrade;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.time.LocalDateTime;

public class GradeRepositoryXML extends  AbstractXMLRepository {
    public GradeRepositoryXML(String fileName) {
        super(fileName);
    }

    @Override
    public Entity createEntityFromElement(Element element) {
        String[] id = element.getAttribute("id").split(" ");
        String idStudent = id[0];
        String idHomework = id[1];
        IdGrade idGrade = new IdGrade(Integer.parseInt(idStudent), Integer.parseInt(idHomework));

        LocalDateTime date = LocalDateTime.parse( element.getElementsByTagName("date")
                .item(0)
                .getTextContent()
        );

        String teacher = element.getElementsByTagName("teacher")
                .item(0)
                .getTextContent();

        int value = Integer.parseInt(element.getElementsByTagName("value")
                .item(0)
                .getTextContent()
        );

        return new Grade(idGrade, date, teacher, value);
    }

    @Override
    public Element createElementFromEntity(Document document, Entity entity) {
        Grade grade = (Grade) entity;

        Element element = document.createElement("grade");
        element.setAttribute("id", grade.getId().toString());

        Element date = document.createElement("date");
        date.setAttribute("date", grade.getDate().toString());
        element.appendChild(date);

        Element teacher = document.createElement("teacher");
        teacher.setAttribute("teacher", grade.getTeacher());
        element.appendChild(teacher);

        Element value = document.createElement("value");
        value.setAttribute("value", String.valueOf(grade.getValue()));
        element.appendChild(value);

        return element;
    }
}
