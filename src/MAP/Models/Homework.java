package MAP.Models;

public class Homework extends Entity<String> {

    private int startWeek;
    private int deadLine;
    private String description;

    public Homework(String id, int startWeek, int deadLine, String description) {
        super(id);
        this.startWeek = startWeek;
        this.deadLine = deadLine;
        this.description = description;
    }

    public int getStartWeek() {
        return startWeek;
    }

    public void setStartWeek(int startWeek) {
        this.startWeek = startWeek;
    }

    public int getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(int deadLine) {
        this.deadLine = deadLine;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Homework{" +
                "startWeek=" + startWeek +
                ", deadLine=" + deadLine +
                ", description='" + description + '\'' +
                '}';
    }
}
