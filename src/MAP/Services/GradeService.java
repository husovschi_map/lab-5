package MAP.Services;

import MAP.Config.ApplicationContext;
import MAP.Models.*;
import MAP.Repositories.Repository;
import MAP.Validators.GradeValidator;
import MAP.Validators.ValidationException;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class GradeService {
    private Repository<IdGrade, Grade> repository;
    private Repository<String, Homework> homeworkRepository;
    private GradeValidator validator;
    private Repository<String, Student> studentRepository;

    public GradeService(Repository<IdGrade, Grade> repository, GradeValidator validator, Repository<String, Homework> homeworkRepository, Repository<String, Student> studentRepository) {
        this.repository = repository;
        this.validator = validator;
        this.homeworkRepository = homeworkRepository;
        this.studentRepository = studentRepository;
    }

    public List<Student> studentsHomework(String idHomework) {
        List<Grade> gradeList =(List<Grade>) this.repository.findAll();

        return gradeList
                .stream()
                .filter(x->x.getId().getIdHomework().equals(idHomework))
                .map(x->studentRepository.findOne(x.getId().getIdStudent()))
                .collect(Collectors.toList());
    }

    public List<Grade> gradesHomeworkAndTeacher(String idHomework, String teacher) {
        List<Grade> gradeList =(List<Grade>) this.repository.findAll();

        return gradeList
                .stream()
                .filter(x->x.getId().getIdHomework().equals(idHomework) && x.getTeacher().equals(teacher))
                .collect(Collectors.toList());
    }

    public List<Grade> gradesHomeworkAndWeek(String idHomework, int week){
        List<Grade> gradeList =(List<Grade>) this.repository.findAll();

        Predicate<Grade> gradePredicate = grade -> grade.getId().getIdHomework().equals(idHomework) &&
                CollegeYear.getInstance().getCurrentWeekOfSemester(grade.getDate()) == week;

        return gradeList
                .stream()
                .filter(gradePredicate)
                .collect(Collectors.toList());
    }

    public int maxGrade(String idHomework, boolean lateTeacher, int assignmentPresentation, Boolean exemption) {
        if (!exemption) {
            int currentWeek = CollegeYear.getInstance().getCurrentWeekOfSemester(LocalDateTime.now());

            if (lateTeacher)
                currentWeek = assignmentPresentation;
            Homework homework = this.homeworkRepository.findOne(idHomework);
            if (homework.getDeadLine() >= currentWeek) return 10;
            if ((currentWeek - homework.getDeadLine()) > 2) return 1;
            return 10 - (currentWeek - homework.getDeadLine());
        }
        return 10;
    }

    public void save(IdGrade idGrade, String teacher, int value, boolean lateTeacher, int assignmentPresentation, Boolean exemption) throws ValidationException {
        int maxGrade = maxGrade(idGrade.getIdHomework(), lateTeacher, assignmentPresentation, exemption);
        if (value > maxGrade){
            throw new ValidationException("Grade bigger than max grade");
        }
        Grade grade = new Grade(idGrade, LocalDateTime.now(), teacher, value);
        this.validator.validate(grade);
        this.repository.save(grade);
    }

    public void update(IdGrade idGrade, String value, String teacher) {
        Grade gradeToUpdate = this.repository.findOne(idGrade);
        if (gradeToUpdate != null) {
            gradeToUpdate.setDate(LocalDateTime.now());
            gradeToUpdate.setTeacher(teacher);
            gradeToUpdate.setValue(Integer.parseInt(value));
        }
    }

    public Grade delete(IdGrade idGrade) {
        return this.repository.delete(idGrade);
    }

    public Grade findOne(IdGrade idGrade) {
        return this.repository.findOne(idGrade);
    }

    public Iterable<Grade> findAll() {
        return this.repository.findAll();
    }

    public void saveToTxtFile(String studentName, String idHomework, String value, int theWeeKWhenTheStudentShowTheAssignment, int deadlineWeek, String feedback) {
        try(PrintWriter printWriter = new PrintWriter(new FileWriter(
                ApplicationContext.getPROPERTIES().getProperty("database.catalog.gradesTXT") + studentName + ".txt"
        ))){
            String line = "";
            line = "Homework: " + idHomework +
                    "Grade: " + value +
                    "The week when the student show the homework: " + theWeeKWhenTheStudentShowTheAssignment +
                    "Deadline:" + deadlineWeek +
                    "Feedback: " + feedback;
            printWriter.println(line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
