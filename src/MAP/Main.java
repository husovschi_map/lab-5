package MAP;

import MAP.Config.ApplicationContext;
import MAP.Presentaion.UI;
import MAP.Repositories.XML.GradeRepositoryXML;
import MAP.Repositories.XML.HomeworkRepositoryXML;
import MAP.Repositories.XML.StudentRepositoryXML;
import MAP.Repositories.XML.TeacherRepositoryXML;
import MAP.Services.GradeService;
import MAP.Services.HomeworkService;
import MAP.Services.StudentService;
import MAP.Services.TeacherService;
import MAP.Validators.*;

public class Main {

    public static void main(String[] args) throws ValidationException {
        StudentValidator studentValidator = new StudentValidator();
        StudentRepositoryXML studentRepositoryXML = new StudentRepositoryXML("./data/student.txt");
        StudentService studentService = new StudentService(studentRepositoryXML, studentValidator);


        HomeworkValidator homeworkValidator = new HomeworkValidator();
        HomeworkRepositoryXML homeworkRepositoryXML = new HomeworkRepositoryXML("./data/homework.txt");
        HomeworkService homeworkService = new HomeworkService(homeworkRepositoryXML, homeworkValidator);

        GradeValidator gradeValidator = new GradeValidator();
        GradeRepositoryXML gradeRepositoryXML = new GradeRepositoryXML("./data/grade.txt");
        GradeService gradeService = new GradeService(gradeRepositoryXML, gradeValidator, homeworkRepositoryXML, studentRepositoryXML);

        TeacherValidator teacherValidator = new TeacherValidator();
        TeacherRepositoryXML teacherRepositoryXML = new TeacherRepositoryXML("./data/teacher.txt");
        TeacherService teacherService = new TeacherService(teacherRepositoryXML, teacherValidator);

        UI ui = new UI(gradeService, homeworkService, studentService, teacherService);
        ui.run();

        //implement the dates from StuctureSemester must have reading from a file (MAP.config.propriets)

        //implement Persistenta datelor: in fisier XML folosind DOM parser (vezi cursul 6)
        //de facut repo cu XML pe fiecare entity
    }
}
