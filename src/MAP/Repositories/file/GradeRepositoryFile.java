package MAP.Repositories.file;

import MAP.Models.Grade;
import MAP.Models.IdGrade;

import java.io.*;
import java.time.LocalDateTime;

public class GradeRepositoryFile extends AbstractFileRepository<IdGrade, Grade> {

    public GradeRepositoryFile(String fileName) {
        super(fileName);
    }

    @Override
    public void saveToFile() {
        try (PrintWriter printWriter = new PrintWriter(new FileWriter(this.fileName))) {
            for (Grade grade : this.findAll()) {
                String line = grade.getId().getIdStudent() + "," +
                        grade.getId().getIdHomework() + "," +
                        grade.getDate() + "," +
                        grade.getTeacher() + "," +
                        grade.getValue();
                printWriter.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadFromFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {
            String line;
            int lineCounter = 0;
            while ((line = bufferedReader.readLine()) != null) {
                lineCounter++;
                String[] parameters = line.split("[,]");
                if (parameters.length != 4) {
                    System.out.println(
                            "The line " +
                                    String.valueOf(lineCounter) +
                                    " from the file " +
                                    this.fileName + "is invalid"
                    );
                    continue;
                } else {
                    IdGrade idGrade = new IdGrade(
                            Integer.parseInt(parameters[0]),
                            Integer.parseInt(parameters[1]));

                    LocalDateTime date = LocalDateTime.parse(parameters[1]);
                    String teacher = parameters[2];
                    int value = Integer.parseInt(parameters[3]);

                    Grade grade = new Grade(idGrade, date, teacher, value);
                    this.map.put(grade.getId(), grade);
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
