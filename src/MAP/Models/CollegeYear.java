package MAP.Models;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class CollegeYear {
    private Semester firstSemester;
    private Semester secondSemester;

    private static CollegeYear instance = new CollegeYear();

    private CollegeYear() {
        firstSemester = new Semester("1");
        secondSemester = new Semester("2");
    }

    public static CollegeYear getInstance() {
        return instance;
    }

    public Semester getFirstSemester() {
        return firstSemester;
    }

    public void setFirstSemester(Semester firstSemester) {
        this.firstSemester = firstSemester;
    }

    public Semester getSecondSemester() {
        return secondSemester;
    }

    public void setSecondSemester(Semester secondSemester) {
        this.secondSemester = secondSemester;
    }

    public int getCurrentWeekOfSemester(LocalDateTime date) {
        Semester semester = getSemesterByDate(date);
        TemporalField weekOfYear = WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear();
        int currentWeek = date.get(weekOfYear);
        int startHolidayWeek = semester.getStartHoliday().get(weekOfYear);
        int startSemesterWeek = semester.getStartSemester().get(weekOfYear);
        int endHolidayWeek = semester.getEndHoliday().get(weekOfYear);

        if (currentWeek < startHolidayWeek)  // return current week in semester
            return currentWeek - startSemesterWeek + 1;
        else
            return currentWeek - startSemesterWeek + endHolidayWeek - startHolidayWeek;
    }

    public int getCurrentWeekOfSemester() {
        return getCurrentWeekOfSemester(LocalDateTime.now());
    }
    private Semester getSemesterByDate(LocalDateTime date) {
        return firstSemester.getEndSemester().isAfter(date.toLocalDate()) ? firstSemester : secondSemester;
    }
}
