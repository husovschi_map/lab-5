package MAP.Models;

public class IdGrade {
    private int idStudent;
    private int idHomework;

    public IdGrade(int idStudent, int idHomework) {
        this.idStudent = idStudent;
        this.idHomework = idHomework;
    }

    public String getIdStudent() { return String.valueOf(idStudent); }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public String getIdHomework() { return String.valueOf(idHomework); }

    public void setIdHomework(int idHomework) {
        this.idHomework = idHomework;
    }

    @Override
    public String toString() {
        return idStudent + " " + idHomework;
    }
}
