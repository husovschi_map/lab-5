package MAP.Repositories.XML;

import MAP.Models.Entity;
import MAP.Models.Student;
import MAP.Models.Teacher;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TeacherRepositoryXML extends AbstractXMLRepository {
    public TeacherRepositoryXML(String fileName) {
        super(fileName);
    }

    @Override
    public Entity createEntityFromElement(Element element) {
        String id = element.getElementsByTagName("id")
                .item(0)
                .getTextContent();

        String firstName = element.getElementsByTagName("firstName")
                .item(0)
                .getTextContent();

        String lastName = element.getElementsByTagName("lastName")
                .item(0)
                .getTextContent();

        String email = element.getElementsByTagName("email")
                .item(0)
                .getTextContent();

        return new Teacher(id, firstName, lastName, email);
    }

    @Override
    public Element createElementFromEntity(Document document, Entity entity) {
        //    public Teacher(String id, String firstName, String lastName, String email) {
        Student student = (Student) entity;

        Element element = document.createElement("id");
        element.setAttribute("id", student.getId());

        Element firstName = document.createElement("firstName");
        firstName.setAttribute("firstName", student.getFirstName());
        element.appendChild(firstName);

        Element lastName = document.createElement("lastName");
        lastName.setAttribute("lastName", student.getFirstName());
        element.appendChild(lastName);

        Element email = document.createElement("email");
        email.setAttribute("email", student.getFirstName());
        element.appendChild(email);

        return element;
    }
}
