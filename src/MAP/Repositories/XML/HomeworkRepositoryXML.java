package MAP.Repositories.XML;

import MAP.Models.Entity;
import MAP.Models.Homework;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class HomeworkRepositoryXML extends AbstractXMLRepository {
    public HomeworkRepositoryXML(String fileName) {
        super(fileName);
    }

    @Override
    public Entity createEntityFromElement(Element element) {
        String id = element.getElementsByTagName("id")
                .item(0)
                .getTextContent();

        int startWeek = Integer.parseInt(element.getElementsByTagName("startWeek")
                .item(0)
                .getTextContent()
        );

        int deadLine = Integer.parseInt(element.getElementsByTagName("deadLine")
                .item(0)
                .getTextContent()
        );

        String description = element.getElementsByTagName("description")
                .item(0)
                .getTextContent();

        return new Homework(id, startWeek, deadLine, description);
    }

    @Override
    public Element createElementFromEntity(Document document, Entity entity) {
        Homework homework = (Homework) entity;

        Element element = document.createElement("id");
        element.setAttribute("id", homework.getId());

        Element startWeek = document.createElement("startWeek");
        startWeek.setAttribute("startWeek", String.valueOf(homework.getStartWeek()));
        element.appendChild(startWeek);

        Element deadline = document.createElement("deadline");
        deadline.setAttribute("deadline", String.valueOf(homework.getDeadLine()));
        element.appendChild(deadline);

        Element description = document.createElement("description");
        description.setAttribute("description", homework.getDescription());
        element.appendChild(description);

        return element;
    }
}
