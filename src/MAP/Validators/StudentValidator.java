package MAP.Validators;

import MAP.Models.Student;


public class StudentValidator implements Validator<Student> {
    @Override
    public void validate(Student student) throws ValidationException {
        String string = "";

        if (student.getId().equals(""))
            string += "Incorect data! ID is invalid\n";

        if (student.getLastName().equals(""))
            string += "Incorect data! Nume is invalid\n";

        if (student.getFirstName().equals(""))
            string += "Incorect data! Prenume is invalid\n";

        if (student.getGroup().equals(""))
            string += "Incorect data! Grupa is invalid\n";

        if (student.getEmail().equals(""))
            string += "Incorect data! Email is invalid\n";

        if (student.getLabTeacher().equals(""))
            string += "Incorect data! CadruDidacticIndrumatorLab is invalid\n";

        if (string.length() > 0)
            throw new ValidationException(string);
    }
}
